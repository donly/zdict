//
//  String+Ext.swift
//  ZDict
//
//  Created by Dong on 16/8/16.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import Foundation

extension String {
    func isStringEmpty() -> Bool
    {
        var returnValue = false
        
        if self.isEmpty  == true
        {
            returnValue = true
            return returnValue
        }
        
        // Make sure user did not submit number of empty spaces
        let stringValue = self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        if(stringValue.isEmpty == true)
        {
            returnValue = true
            return returnValue
            
        }
        
        return returnValue
    }
    
    func matchesForRegexInText(regex: String!) -> [NSTextCheckingResult] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = self as NSString
            let results = regex.matchesInString(self,
                                                options: [], range: NSMakeRange(0, nsString.length))
            //            return results.map { nsString.substringWithRange($0.range)}
            return results
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func stringByAddingPercentEncodingForRFC3986() -> String? {
        let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumericCharacterSet()
        allowed.addCharactersInString(unreserved)
        return stringByAddingPercentEncodingWithAllowedCharacters(allowed)
    }
}