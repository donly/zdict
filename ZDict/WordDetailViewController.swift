//
//  WordDetailViewController.swift
//  ZDict
//
//  Created by Dong on 16/8/16.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import UIKit
import WebKit

class WordDetailViewController: UIViewController {
    var webView: WordDetailView!
    
    var word: Word?
    
    var requestTask: NSURLSessionDataTask?
    var reSearchMenuItem: UIMenuItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "搜索", style: .Done, target: nil, action: nil)
        title = word!.key
        
        reSearchMenuItem = UIMenuItem(title: "反查", action: #selector(reSearchWord))
        
        configureWebview()
        
        updateUI()
        searchWork(word!.objectId)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
        
//        NSNotificationCenter.defaultCenter().addObserver(self,
//                                                         selector: #selector(menuDidShow),
//                                                         name: UIMenuControllerDidShowMenuNotification,
//                                                         object: nil)
        
//        webView.scrollView.subviews.first?.becomeFirstResponder()
//        NSOperationQueue.mainQueue().addOperationWithBlock { 
//            self.becomeFirstResponder()
//        }
//        webView.becomeFirstResponder()
//        var items = UIMenuController.sharedMenuController().menuItems
//        if items == nil {
//            items = [UIMenuItem]()
//        } else {
//            if !items!.contains(reSearchMenuItem) {
//                items!.insert(reSearchMenuItem, atIndex: 0)
//            }
//        }
        
        UIMenuController.sharedMenuController().menuItems = [reSearchMenuItem]
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
//        NSNotificationCenter.defaultCenter().removeObserver(self,
//                                                            name: UIMenuControllerDidShowMenuNotification,
//                                                            object: nil)
        UIMenuController.sharedMenuController().menuItems = nil
        requestTask?.cancel()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    override func presentViewController(viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?) {
        if presentedViewController != nil {
            // Unsure why WKWebView calls this controller - instead of it's own parent controller
            presentedViewController?.presentViewController(viewControllerToPresent, animated: flag, completion: completion)
        } else {
            super.presentViewController(viewControllerToPresent, animated: flag, completion: completion)
        }
    }
    
//    func menuDidShow() {
//        print("menuDidShow")
//        
//    }
    
    @IBAction func addFavorite(sender: AnyObject) {
        if let w = word {
            let result =  FavoriteWord.saveWithWord(w)
            if result != nil {
                updateBarButton()
            }
        }
    }
    
    func removeFavorite() {
        guard let objectId = word?.objectId else {
            return
        }
        guard FavoriteWord.removeWithObjectId(objectId) else {
            return
        }
        
        updateBarButton()
    }
    
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        if action == #selector(reSearchWord) {
            return true
        }
        
        return false
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    func reSearchWord() {
        let js = "getSelectedText();"
        webView.evaluateJavaScript(js) { (response:AnyObject?, error:NSError?) in
            //                print(error)
        }
    }
    
    func configureWebview() {
        let source = "var meta = document.createElement('meta');" +
            "meta.name = 'viewport';" +
            "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
            "var head = document.getElementsByTagName('head')[0];" +
            "head.appendChild(meta);" +
            "function updateImage(old,newSrc){" +
            "var a = document.getElementsByTagName('img');" +
            "for(var i=0;i<a.length;i++){" +
            "var src = a[i].getAttribute('src');" +
            "if(src.indexOf(old)!==-1){" +
            "a[i].setAttribute('src',newSrc)}}};" +
            "function getWordByURL(url){" +
            "window.webkit.messageHandlers.GetWordByURL.postMessage({text: url})" +
            "};" +
            "function getSelectedText(){var rangeText = window.getSelection().toString();" +
            "window.webkit.messageHandlers.GetWordBySelect.postMessage({text: rangeText})};";
        let script: WKUserScript = WKUserScript(source: source, injectionTime: .AtDocumentEnd, forMainFrameOnly: true)
        
        // Create the user content controller and add the script to it
        let userContentController: WKUserContentController = WKUserContentController()
        userContentController.addUserScript(script)
        userContentController.addScriptMessageHandler(self, name: "GetWordByURL")
        userContentController.addScriptMessageHandler(self, name: "GetWordBySelect")
        
        // Create the configuration with the user content controller
        let configuration: WKWebViewConfiguration = WKWebViewConfiguration()
        configuration.userContentController = userContentController
            
        webView = WordDetailView(frame: CGRectZero, configuration: configuration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        webView.becomeFirstResponder()
        view.addSubview(webView)
        
        let consW = NSLayoutConstraint(item: webView, attribute: .Width, relatedBy: .Equal,
                                       toItem: view, attribute: .Width, multiplier: 1.0, constant: 0)
        let consH = NSLayoutConstraint(item: webView, attribute: .Height, relatedBy: .Equal,
                                       toItem: view, attribute: .Height, multiplier: 1.0, constant: 0)
        let consL = NSLayoutConstraint(item: webView, attribute: .Leading, relatedBy: .Equal,
                                       toItem: view, attribute: .Leading, multiplier: 1.0, constant: 0)
        let consT = NSLayoutConstraint(item: webView, attribute: .Top, relatedBy: .Equal,
                                       toItem: view, attribute: .Top, multiplier: 1.0, constant: 0)
        view.addConstraints([consW, consH, consL, consT])
    }
    
    func searchWork(keyword: String?) {
        if keyword!.isStringEmpty() {
            return
        }
        
        if requestTask != nil {
            requestTask?.cancel()
            requestTask = nil
        }
        
        let apiUrl = AppDelegate.appDelegate.schema() + "zdic.paralleltrees.com/entry/"
        let urlWithParams = apiUrl + keyword!
        let myUrl = NSURL(string: urlWithParams.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        guard  myUrl != nil else {
            return
        }
        
        let request = NSMutableURLRequest(URL: myUrl!);
        request.HTTPMethod = "GET"
        
        let urlconfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        urlconfig.timeoutIntervalForRequest = 12
        urlconfig.timeoutIntervalForResource = 12
        let session = NSURLSession(configuration: urlconfig, delegate: nil, delegateQueue: nil)
        
        let task = session.dataTaskWithRequest(request) {
            data, response, error in
            
            guard error == nil else {
                if error?.code != NSURLErrorCancelled {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                }
                
                print("error=\(error)")
                return
            }
            
            // Print out response string
//            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            //print("responseString = \(responseString)")
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            // Convert server json response to NSDictionary
            do {
                if let obj = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    //print(obj)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        if self.word?.objectId == keyword {
                            let content = self.word?.content
                            self.word?.content = obj["content"] as? String
                            self.word?.save()
                            if content == nil {
                                self.updateUI()
                            }
                        }
                        else {
                            let detailVC = self.storyboard?.instantiateViewControllerWithIdentifier("WordDetailViewController") as! WordDetailViewController
                            detailVC.word = Word.saveModel(obj)
                            self.navigationController?.pushViewController(detailVC, animated: true)
                        }
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        requestTask = task
        task.resume()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func searchPrefix(keyword: String?) {
        if keyword!.isStringEmpty() {
            return
        }
        
        if requestTask != nil && requestTask?.state == .Running {
            requestTask?.cancel()
            requestTask = nil
        }
        
        let apiUrl = AppDelegate.appDelegate.schema() + "zdic.paralleltrees.com/entry"
        let urlWithParams = apiUrl + "?skip=0&limit=1&prefix=\(keyword!)"
        let myUrl = NSURL(string: urlWithParams.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        guard  myUrl != nil else {
            return
        }
        
        let request = NSMutableURLRequest(URL: myUrl!);
        request.HTTPMethod = "GET"
        
        let urlconfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        urlconfig.timeoutIntervalForRequest = 12
        urlconfig.timeoutIntervalForResource = 12
        let session = NSURLSession(configuration: urlconfig, delegate: nil, delegateQueue: nil)
        
        let task = session.dataTaskWithRequest(request) {
            data, response, error in
            
            guard error == nil else {
                if error?.code != NSURLErrorCancelled {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                }
                print("error=\(error)")
                return
            }
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
            // Print out response string
            //            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            //            print("responseString = \(responseString)")
            
            
            // Convert server json response to NSDictionary
            do {
                if let obj = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSArray {
                    for dict in obj {
                        if let word = Word.saveModel(dict as! NSDictionary) {
                            self.searchWork(word.objectId)
                            break
                        }
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
                dispatch_async(dispatch_get_main_queue(), {
                    let av = UIAlertController(title: "错误", message: error.localizedDescription,
                        preferredStyle: .Alert)
                    av.addAction(UIAlertAction(title: "重试", style: .Default, handler: { (action: UIAlertAction) in
                        AppDelegate.appDelegate.useHttp = !AppDelegate.appDelegate.useHttp
                        self.searchPrefix(keyword)
                    }))
                    av.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: { (action: UIAlertAction) in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }))
                    self.presentViewController(av, animated: true, completion: nil)
                })
            }
        }
        task.resume()
        requestTask = task
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func updateBarButton() {
        if let objectId = word?.objectId {
            let favor = FavoriteWord.modelWithObjectId(objectId)
            
            var barButton = UIBarButtonItem(barButtonSystemItem: .Add,
                                            target: self,
                                            action: #selector(WordDetailViewController.addFavorite(_:)))
            if favor != nil {
                // remove
                barButton = UIBarButtonItem(barButtonSystemItem: .Trash,
                                            target: self,
                                            action: #selector(WordDetailViewController.removeFavorite))
            }
            
            navigationItem.setRightBarButtonItem(barButton, animated: true)
        }
    }
    
    func updateUI() {
        updateBarButton()
        
        guard let content = word!.content else {
            return
        }
        if let regex = try? NSRegularExpression(pattern: "alt=\".+?\"", options: .CaseInsensitive) {
            let modString = regex.stringByReplacingMatchesInString(content, options: .WithTransparentBounds, range: NSMakeRange(0, content.characters.count), withTemplate: "alt=\"\"")
            //print(modString)
            webView.loadHTMLString(modString, baseURL: nil)
        } else {
            webView.loadHTMLString(content, baseURL: nil)
        }
    }
    
    func updateWebView(object: NSDictionary) {
        let key = object["key"]
        let imageUrl = "data:image/gif;base64," + (object["content"] as! String)
        let js = "updateImage('\(key!.stringByReplacingOccurrencesOfString("\\", withString: "/"))','\(imageUrl)');"
        webView.evaluateJavaScript(js) { (response:AnyObject?, error:NSError?) in
            //print(error)
            self.webView.becomeFirstResponder()
        }
    }
    
    func downloadImage(imagePath: String?) {
        if imagePath!.isStringEmpty() {
            return
        }
        
        let apiUrl = AppDelegate.appDelegate.schema() + "zdic.paralleltrees.com/media"
        let urlWithParams = apiUrl + "?key=\(imagePath!.stringByAddingPercentEncodingForRFC3986()!)"
        let myUrl = NSURL(string: urlWithParams)
        guard  myUrl != nil else {
            return
        }
        
        let request = NSMutableURLRequest(URL: myUrl!);
        request.HTTPMethod = "GET"
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            // Check for error
            if error != nil
            {
                //print("error=\(error)")
                return
            }

            // Convert server json response to NSDictionary
            do {
                if let obj = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    //print(obj)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.updateWebView(obj)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    func downloadImages() {
        guard let content = word?.content else {
            return;
        }
        
        let pattern = "<img.*?src=\"file.+?>"
        let result = content.matchesForRegexInText(pattern)
        let nsContent = content as NSString
        var urlList = [String]()
        
        for regResult in result {
            let imageTag = nsContent.substringWithRange(regResult.range) as NSString
            let fileSchema = "file://"
            let range = imageTag.rangeOfString(fileSchema)
            let urlrange = NSRange.init(location: range.location+fileSchema.characters.count,
                                        length: imageTag.length-3-range.location-fileSchema.characters.count)
            let url = imageTag.substringWithRange(urlrange)
            if urlList.contains(url) {
                continue
            }
            
            urlList.append(url)
            downloadImage(url.stringByReplacingOccurrencesOfString("/", withString: "\\"))
        }
    }
}

extension WordDetailViewController: WKNavigationDelegate {
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        //print("didFinishNavigation")
        downloadImages()
        let js = "document.body.style.webkitTouchCallout='none';"
        webView.evaluateJavaScript(js) { (response:AnyObject?, error:NSError?) in
//                            print(error)
        }
    }
    
    func webView(webView: WKWebView, decidePolicyForNavigationAction navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        guard let scheme = navigationAction.request.URL?.scheme else {
            return decisionHandler(.Cancel)
        }
        
        if scheme == "entry" {
            guard let host = navigationAction.request.URL?.host else {
                return decisionHandler(.Cancel)
            }
//            let punycode = Punycode.official
//            
//            let js = "getWordByURL('\(punycode.decode(host))');"
//            webView.evaluateJavaScript(js) { (response:AnyObject?, error:NSError?) in
////                print(error)
//            }
            let word = host.IDNADecodedString()
            print("IDNA decode is \(word)")
            searchPrefix(word)
            return decisionHandler(.Cancel)
        }
        
        return decisionHandler(.Allow)
    }
    
}

extension WordDetailViewController: WKScriptMessageHandler {
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        if message.name == "GetWordBySelect" {
            if let word = message.body["text"] {
                searchPrefix(word as? String)
            }
        }
    }
}