//
//  WordModel.swift
//  ZDict
//
//  Created by Dong on 16/8/16.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import Foundation

struct WordModel {
    var key: String
    var objectId: String
    var content: String
    var update: NSDate?
//    init(key: String, objectId: String) {
//        self.key = key
//        self.objectId = objectId
//    }
}