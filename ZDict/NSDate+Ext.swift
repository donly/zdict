//
//  NSDate+Ext.swift
//  ZDict
//
//  Created by Dong on 16/8/17.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import Foundation

extension NSDate {
    public var timeAgo: String {
        
        // TODO: localize for other calanders
//        if components.day >= 7 {
//            let week = components.day/7
//            if week < 2 {
//                return "上周"
//            } else {
//                return "\(week) 周之前"
//            }
//        }
        
        
        if NSCalendar.currentCalendar().isDateInToday(self) {
            return NSDate.Formatter.time.stringFromDate(self)
        }
        if NSCalendar.currentCalendar().isDateInYesterday(self) {
            return "昨天"
        }
        
//        if components.hour > 0 {
//            if components.hour < 2 {
//                return NSDateTimeAgoLocalizedStrings("An hour ago")
//            } else  {
//                return stringFromFormat("%%d %@hours ago", withValue: components.hour)
//            }
//        }
        
//        if components.minute > 0 {
//            if components.minute < 2 {
//                return NSDateTimeAgoLocalizedStrings("A minute ago")
//            } else {
//                return stringFromFormat("%%d %@minutes ago", withValue: components.minute)
//            }
//        }
//        
//        if components.second > 0 {
//            if components.second < 15 {
//                return "刚刚"
//            }
//        }
        
        return NSDate.Formatter.date.stringFromDate(self)
    }
    
    private func dateComponents() -> NSDateComponents {
        let calander = NSCalendar.currentCalendar()
        return calander.components([.Second, .Minute, .Hour, .Day, .Month, .Year], fromDate: self, toDate: NSDate(), options: [])
    }
    
    struct Formatter {
        static let date = NSDateFormatter(dateFormat: "yyyy-MM-dd")
        static let time = NSDateFormatter(dateFormat: "HH:mm")
    }
}

extension NSDateFormatter {
    convenience init(dateFormat: String) {
        self.init()
        self.dateFormat =  dateFormat
        self.locale = NSLocale.currentLocale()
    }
}
