//
//  FirstViewController.swift
//  ZDict
//
//  Created by Don on 16/8/16.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import UIKit
import CoreData

class FirstViewController: UITableViewController {
    // 是否搜索
    var isDataFiltered: Bool = false
    // 搜索内容
    var searchPrefix: String = ""
    
    var historyArray: [HistoryWord]?
    var prefixArray: [Word]?
    var prefixPage: Int?
    let pageSize = 10
    var prefixLoadAll = false
    var requestTask: NSURLSessionDataTask?
    
    var favoriteArray: [FavoriteWord]?
    
    lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        self.definesPresentationContext = true
        return searchController
    }()
    
    @IBOutlet var aboutButton: UIButton!
    
    lazy var fetchedResultsController: NSFetchedResultsController = {
        let request = NSFetchRequest.init(entityName: "FavoriteWord")
        
        request.sortDescriptors = [NSSortDescriptor(key: "created", ascending: false)]
        let context = AppDelegate.appDelegate.managedObjectContext
        let fetchedResultsController = NSFetchedResultsController.init(fetchRequest: request,
                                                                       managedObjectContext: context,
                                                                       sectionNameKeyPath: nil,
                                                                       cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableHeaderView = searchController.searchBar
        tableView.registerNib(UINib.init(nibName: "SearchResultCell", bundle: nil),
                              forCellReuseIdentifier: "HistoryCell")
        tableView.registerNib(UINib.init(nibName: "SearchResultCell", bundle: nil),
                              forCellReuseIdentifier: "SearchTableCell")
        tableView.registerNib(UINib(nibName: "LoadMoreCell", bundle: nil),
                              forCellReuseIdentifier: "LoadMoreCell")
        
        let aboutButtonItem = UIBarButtonItem(customView: self.aboutButton)
        self.navigationItem.setRightBarButtonItem(aboutButtonItem, animated: true)
        
        prefixPage = 0
        prefixArray = [Word]()
        loadFavoriteList()
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(FirstViewController.dayChanged(_:)),
                                                         name: UIApplicationSignificantTimeChangeNotification,
                                                         object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dayChanged(notification: NSNotification){
        tableView.reloadData()
    }
    
    /**
     加载收藏列表
     */
    func loadFavoriteList() {
        do {
            try fetchedResultsController.performFetch()
            favoriteArray = fetchedResultsController.fetchedObjects as? [FavoriteWord]
            tableView.reloadData()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func search(sender: AnyObject) {
        searchController.active = true
    }
    
    @IBAction func about(sender: AnyObject) {
        //First get the nsObject by defining as an optional anyObject
        let nsObject: AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"]
        
        //Then just cast the object as a String, but be careful, you may want to double check for nil
        let version = nsObject as! String
        
        let av = UIAlertController(title: "版本 \(version)",
                                   message: "词典内容来自汉典（zdic.net）\n遵循 CC0 1.0 Public Domain Dedication",
                                   preferredStyle: .ActionSheet)
        
        av.addAction(UIAlertAction(title: "确定", style: .Cancel, handler: { (action: UIAlertAction) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        av.view.tintColor = UINavigationBar.appearance().tintColor
        
        if let popover = av.popoverPresentationController {
            popover.barButtonItem = self.navigationItem.rightBarButtonItem;
        }

        self.presentViewController(av, animated: true, completion: nil)
    }
    
    
    /**
     加载历史列表
     */
    func loadHistoryList() {
        let context = AppDelegate.appDelegate.managedObjectContext
        
        let request = NSFetchRequest.init(entityName: "HistoryWord")
        request.sortDescriptors = [NSSortDescriptor(key: "created", ascending: false)]
        do {
            historyArray = try context.executeFetchRequest(request) as? [HistoryWord]
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        tableView.reloadData()
    }
    
    func searchPrefix(keyword: String?) {
        if keyword!.isStringEmpty() {
            return
        }
        
        if requestTask != nil && requestTask?.state == .Running {
            requestTask?.cancel()
            requestTask = nil
        }
        
        let apiUrl = AppDelegate.appDelegate.schema() + "zdic.paralleltrees.com/entry"
        let urlWithParams = apiUrl + "?skip=\(prefixPage!)&limit=\(pageSize)&prefix=\(keyword!)"
        let myUrl = NSURL(string: urlWithParams.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        guard  myUrl != nil else {
            return
        }
        
        let request = NSMutableURLRequest(URL: myUrl!);
        request.HTTPMethod = "GET"
        
        let urlconfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        urlconfig.timeoutIntervalForRequest = 12
        urlconfig.timeoutIntervalForResource = 12
        let session = NSURLSession(configuration: urlconfig, delegate: nil, delegateQueue: nil)

        let task = session.dataTaskWithRequest(request) {
            data, response, error in
            
            if let error = error {
                if error.code != NSURLErrorCancelled {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        let av = UIAlertController(title: "错误", message: error.localizedDescription,
                            preferredStyle: .Alert)
                        av.addAction(UIAlertAction(title: "重试", style: .Default, handler: { (action: UIAlertAction) in
                            AppDelegate.appDelegate.useHttp = !AppDelegate.appDelegate.useHttp
                            self.searchPrefix(keyword)
                        }))
                        av.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: { (action: UIAlertAction) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }))
                        self.presentViewController(av, animated: true, completion: nil)
                        self.tableView.reloadData()
                    })
                }
                
                print("error=\(error)")
                return
            }
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
            // Print out response string
//            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//            print("responseString = \(responseString)")
            
            
            // Convert server json response to NSDictionary
            do {
                if let obj = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSArray {
                    for dict in obj {
                        if let word = Word.saveModel(dict as! NSDictionary) {
                            self.prefixArray?.append(word)
                        }
                    }

                    self.prefixLoadAll = (obj.count == 0) || (obj.count < self.pageSize)
                    dispatch_async(dispatch_get_main_queue(), {
                        self.tableView.reloadData()
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
                dispatch_async(dispatch_get_main_queue(), { 
                    let av = UIAlertController(title: "错误", message: error.localizedDescription,
                        preferredStyle: .Alert)
                    av.addAction(UIAlertAction(title: "重试", style: .Default, handler: { (action: UIAlertAction) in
                        AppDelegate.appDelegate.useHttp = !AppDelegate.appDelegate.useHttp
                        self.searchPrefix(keyword)
                    }))
                    av.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: { (action: UIAlertAction) in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }))
                    self.presentViewController(av, animated: true, completion: nil)
                    
                    self.tableView.reloadData()
                })
            }
        }
        task.resume()
        requestTask = task
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func resetPrefixSearch() {
        prefixPage = 0
        prefixArray?.removeAll()
        self.prefixLoadAll = false
    }
    
    func searchIsEmpty() -> Bool {
        if let searchTerm = self.searchController.searchBar.text {
            return searchTerm.isEmpty
        }
        return true
    }
}

// MARK: - UISearchControllerDelegate
extension FirstViewController: UISearchControllerDelegate {
    func willPresentSearchController(searchController: UISearchController) {
        print("willPresentSearchController")
        searchPrefix = ""
        loadHistoryList()
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        print("didDismissSearchController")
        requestTask?.cancel()
        requestTask = nil
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        tableView.reloadData()
    }
}

// MARK: - UISearchResultsUpdating
extension FirstViewController: UISearchResultsUpdating {
    func filterData() {
        if searchIsEmpty() {
            searchPrefix = ""
            isDataFiltered = false
        } else {
            guard let searchText = self.searchController.searchBar.text else {
                return
            }
            
            if searchPrefix == searchText {
                return
            }
            
            searchPrefix = searchText
            isDataFiltered = true
            resetPrefixSearch()
            searchPrefix(self.searchController.searchBar.text)
        }
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        print("updateSearchResultsForSearchController")
        
        filterData()
        tableView.reloadData()
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension FirstViewController: NSFetchedResultsControllerDelegate {
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        print("didChangeObject")
        if searchController.active {
            loadFavoriteList()
        }
    }
}

extension FirstViewController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.active {
            guard isDataFiltered else {
                if let count = historyArray?.count {
                    return count
                }
                return 0
            }
            
            guard let count = prefixArray?.count else {
                return 0
            }
            
            if count > 0 && !self.prefixLoadAll {
                return count + 1
            }
            
            return count
        }
        
        if let count = favoriteArray?.count {
            return count
        }
        return 0;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if searchController.active {
            guard isDataFiltered else {
                let cell = tableView.dequeueReusableCellWithIdentifier("HistoryCell", forIndexPath: indexPath)
                let history = historyArray![indexPath.row]
                cell.textLabel?.text = history.key
                return cell
            }
            
            if indexPath.row == prefixArray!.count {
                // load more cell
                let cell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell", forIndexPath: indexPath)
                return cell
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("SearchTableCell", forIndexPath: indexPath)
            let word = prefixArray![indexPath.row]
            cell.textLabel?.text = word.key
            
            return cell;
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("TableCell", forIndexPath: indexPath)
        let favorite = favoriteArray![indexPath.row]
        cell.textLabel?.text = favorite.key
        
        cell.detailTextLabel?.text = favorite.created!.timeAgo
        return cell;
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if searchController.active {
            guard isDataFiltered else {
                let word = historyArray![indexPath.row] as Word
                searchController.searchBar.text = word.key
                resetPrefixSearch()
                searchPrefix(word.key)
                performSegueWithIdentifier("WordDetailViewController", sender: word)
                return
            }
            
            if indexPath.row == prefixArray!.count {
                // load more request
                prefixPage! += pageSize
                searchPrefix(self.searchController.searchBar.text)
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
                return
            }
            
            let word = prefixArray![indexPath.row]
            HistoryWord.saveHistory(word)
            loadHistoryList()
            performSegueWithIdentifier("WordDetailViewController", sender: word)
            return
        }
        
        let word = favoriteArray![indexPath.row]
        performSegueWithIdentifier("WordDetailViewController", sender: word)
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.active {
            guard isDataFiltered else {
                return "搜索历史"
            }
            
            if requestTask?.state == .Running {
                return "正在搜索..."
            }
        } else {
            return "我的收藏"
        }
        
        return nil
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if searchController.active {
            guard isDataFiltered else {
                return true
            }
            
            return false
        }
        
        return true
    }
    
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        if searchController.active {
            guard isDataFiltered else {
                return .Delete
            }
            
            return .None
        }
        
        return .Delete
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            guard searchController.active else {
                // 删除收藏
                let word = favoriteArray![indexPath.row]
                guard let objectId = word.objectId else {
                    return
                }
                
                if FavoriteWord.removeWithObjectId(objectId) {
                    print("删除收藏成功！")
                    favoriteArray?.removeAtIndex(indexPath.row)
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                    return
                }
                
                return
            }
            
            guard isDataFiltered else {
                // 删除搜索历史
                print("删除搜索历史")
                let word = historyArray![indexPath.row] as HistoryWord
                if word.deleteModel() {
                    historyArray?.removeAtIndex(indexPath.row)
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                } else {
                    print("删除失败！")
                }
                
                return
            }
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "WordDetailViewController" {
            let word = sender as! Word
            let vc = segue.destinationViewController as! WordDetailViewController
            vc.word = word
        }
    }
}
