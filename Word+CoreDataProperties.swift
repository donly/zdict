//
//  Word+CoreDataProperties.swift
//  ZDict
//
//  Created by Don on 16/8/17.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Word {

    @NSManaged var key: String?
    @NSManaged var objectId: String?
    @NSManaged var update: NSDate?
    @NSManaged var content: String?

}
