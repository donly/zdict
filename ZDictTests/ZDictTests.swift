//
//  ZDictTests.swift
//  ZDictTests
//
//  Created by Don on 16/8/16.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import XCTest
@testable import ZDict

class ZDictTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testLog() {
        let string = "Hello, world!"
        let url = "http://www.andyyhope.com"
        let date = NSDate()
        let any = ["Key": 2]
        let errorMsg = NSError(domain: "404 Not Found!", code: 404, userInfo: nil)
        
        /*
        log.ln(string)/
        log.url(url)/
        log.date(date)/
        log.error(errorMsg)/
        log.any(UIColor.redColor())/
        log.any(any)/
        */
        
        print(String)
        print(string)
        print(url)
        print(date)
        print(any)
        print(errorMsg)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
