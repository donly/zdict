//
//  FavoriteWord.swift
//  ZDict
//
//  Created by Don on 16/8/17.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import Foundation
import CoreData


class FavoriteWord: Word {

// Insert code here to add functionality to your managed object subclass
    override class func modelWithObjectId(objectId: String) -> FavoriteWord? {
        let context = AppDelegate.appDelegate.managedObjectContext
        let request = NSFetchRequest.init(entityName: "FavoriteWord")
        request.predicate = NSPredicate(format: "objectId = %@", objectId)
        
        do {
            let models = try context.executeFetchRequest(request) as? [FavoriteWord]
            if models?.count > 0 {
                return models?.first
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    class func saveWithWord(word: Word) -> FavoriteWord? {
        let context = AppDelegate.appDelegate.managedObjectContext
        
        var model = modelWithObjectId(word.objectId!)
        
        if model == nil {
            model = NSEntityDescription.insertNewObjectForEntityForName("FavoriteWord",
                                                                        inManagedObjectContext: context) as? FavoriteWord
        }
        
        model!.key = word.key
        model!.content = word.content
        model!.objectId = word.objectId
        model!.update = word.update
        model!.created = NSDate()
        
        do {
            try context.save()
            return model
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    class func removeWithObjectId(objectId: String) -> Bool {
        guard let model = modelWithObjectId(objectId) else {
            return false
        }
        
        guard let context = model.managedObjectContext else {
            return false
        }
        
        context.deleteObject(model)
        
        do {
            try context.save()
            return true
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return false
    }
}
