//
//  Word.swift
//  ZDict
//
//  Created by Don on 16/8/17.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import Foundation
import CoreData


class Word: NSManagedObject {
    
    class func saveModel(dict: NSDictionary) -> Word? {
        let context = AppDelegate.appDelegate.managedObjectContext
        
        var model = modelWithObjectId(dict["objectId"] as! String)

        if model == nil {
            model = NSEntityDescription.insertNewObjectForEntityForName("Word",
                                                                        inManagedObjectContext: context) as? Word
        }

        model!.key = dict["key"] as? String
        model!.content = dict["content"] as? String
        model!.objectId = dict["objectId"] as? String
        model!.update = nil
        
        do {
            try context.save()
            return model
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    class func modelWithObjectId(objectId: String) -> Word? {
        let context = AppDelegate.appDelegate.managedObjectContext
        let request = NSFetchRequest.init(entityName: "Word")
        request.predicate = NSPredicate(format: "objectId = %@", objectId)

        do {
            let models = try context.executeFetchRequest(request) as? [Word]
            if models?.count > 0 {
                return models?.first
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    func save() -> Bool {
        do {
            try self.managedObjectContext?.save()
            return true
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return false
    }
}
