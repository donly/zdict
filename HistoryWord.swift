//
//  HistoryWord.swift
//  ZDict
//
//  Created by Don on 16/8/17.
//  Copyright © 2016年 MAGICALBOY. All rights reserved.
//

import Foundation
import CoreData


class HistoryWord: Word {

// Insert code here to add functionality to your managed object subclass
    override class func modelWithObjectId(objectId: String) -> HistoryWord? {
        let context = AppDelegate.appDelegate.managedObjectContext
        let request = NSFetchRequest.init(entityName: "HistoryWord")
        request.predicate = NSPredicate(format: "objectId = %@", objectId)
        
        do {
            let models = try context.executeFetchRequest(request) as? [HistoryWord]
            if models?.count > 0 {
                return models?.first
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    /**
     保存搜索历史
     
     - parameter word: 搜索的单词
     
     - returns: 保存是否成功
     */
    class func saveHistory(word: Word) -> Bool {
        let context = AppDelegate.appDelegate.managedObjectContext
        var model: HistoryWord?
        
        model = HistoryWord.modelWithObjectId(word.objectId!)
        
        if model == nil {
            model = NSEntityDescription.insertNewObjectForEntityForName("HistoryWord",
                                                                        inManagedObjectContext: context) as? HistoryWord
        }
        
        model!.key = word.key
        model!.content = word.content
        model!.objectId = word.objectId
        model!.update = word.update
        model!.created = NSDate.init()
        
        var result = false
        do {
            try context.save()
            result = true
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return result
    }
    
    func deleteModel() -> Bool {
        if let context = self.managedObjectContext {
            context.deleteObject(self)
            do {
                try context.save()
                return true
            } catch let error as NSError {
                print(error.localizedDescription)
                return false
            }
        }
        
        return false
    }
}
